export class Task {
    description: string;
    isDone: boolean;
    constructor() {
        this.description = '';
        this.isDone = false;
    }
}