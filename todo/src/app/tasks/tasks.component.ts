import { Task } from './../model/task.model';
import { TaskService } from './../services/task.service';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.css']
})
export class TasksComponent implements OnInit {
  tasks: Task[]
  constructor(private taskServices: TaskService) {
    this.tasks = this.taskServices.onGet();
    console.log(this.tasks)
  }

  ngOnInit(): void {
  }

  onSubmit(form: NgForm) {


    let task: Task = {
      description: form.value.description,
      isDone: form.value.isDone
    }
  }
}
