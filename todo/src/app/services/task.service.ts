import { Task } from '../model/task.model';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TaskService {
  tasks: Task[] = [
    {
      description: 'Java',
      isDone: false
    },
    {
      description: 'python',
      isDone: false
    }
  ]
  constructor() { }
  onGet() {
    return this.tasks;
  }

}
